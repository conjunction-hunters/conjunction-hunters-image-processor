import cv2
import numpy as np
import os
import shutil
from datetime import datetime
from tqdm import tqdm

# Set the directory path
directory = '\conjunction-hunters-image-processor\Images'

# Create a dictionary to store the date and corresponding images
date_images = {}

try:
    # Loop through each file in the directory
    for filename in tqdm(os.listdir(directory), desc="Processing files"):
        # Check if the file is a .PNG image
        if filename.endswith('.png'):
            full_path = os.path.join(directory, filename)
            # Get the file modification date
            timestamp = os.path.getmtime(full_path)
            date = datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d')

            # Add the image to the list of images for the corresponding date
            if date not in date_images:
                date_images[date] = []
            date_images[date].append(full_path)
except Exception as e:
    print(f"Error processing files: {e}")

# Loop through each date and its corresponding images
for date, images in tqdm(date_images.items(), desc="Processing images"):
    # Create a directory for the date if it doesn't exist
    date_directory = os.path.join(directory, 'Trails-' + date)
    if not os.path.exists(date_directory):
        os.makedirs(date_directory)

    for image_path in images:
        try:
            # Move the original images to the date directory
            shutil.move(image_path, date_directory)
        except Exception as e:
            print(f"Error moving image {image_path}: {e}")

    # Process the images (apply Hough transform and draw lines)
    for filename in os.listdir(date_directory):
        try:
            # Load the image
            img_path = os.path.join(date_directory, filename)
            img = cv2.imread(img_path)

            # Convert to grayscale
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # Apply thresholding to segment out the trails
            _, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)

            # Apply the Hough transform to detect lines
            minLineLength = 100
            maxLineGap = 10
            lines = cv2.HoughLinesP(thresh, 1, np.pi/180, 200, minLineLength, maxLineGap)

            # Draw the detected lines on the original image
            if lines is not None:
                for line in lines:
                    x1, y1, x2, y2 = line[0]
                    cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 2)

            # Save the output image
            cv2.imwrite(os.path.join(date_directory, 'output_' + filename), img)
        except Exception as e:
            print(f"Error processing image {img_path}: {e}")