# Conjunction Hunters Image Processor



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/conjunction-hunters/conjunction-hunters-image-processor.git
git branch -M main
git push -uf origin main
```

SatTrails Script
================

The SatTrails script is designed to detect satellite trails in astrophotography images. It processes a specified directory and its subdirectories, applying thresholding and the Hough transform to identify lines in the images. The detected lines are then drawn on the original images, which are saved with "output_" prefixed to their original names.

**Introduction:**
Satellite trails can be challenging to detect in astrophotography images due to noise and other visual contaminants. This script uses a combination of thresholding and the Hough transform to identify and draw lines corresponding to satellite trails in the images.

**Usage:**

1. Place the script in the root directory of your image files.
2. Update the `directory` variable at the top of the script to point to the directory containing your astrophotography images.
3. Run the script using a Python interpreter or an IDE like PyCharm.

**Notes:**

* This script assumes that all images are .PNG files and are located in the specified directory or its subdirectories.
* You can adjust the threshold values and Hough transform parameters to optimize line detection for your specific image set.
* The output images will be saved with "output_" prefixed to their original names, so they won't overwrite the original images.

**Script Overview:**

The script consists of three main parts:

1. **Image Processing:** The script loops through each image file in the specified directory and its subdirectories, checking if it's a .PNG image. For each image, it gets the modification date and adds it to a dictionary with corresponding images.
2. **Image Organization:** The script organizes the images by date, moving them to separate directories for each day.
3. **Line Detection:** The script applies thresholding to segment out the trails in each image, then uses the Hough transform to detect lines. The detected lines are drawn on the original images using OpenCV's `line` function.

**Dependencies:*